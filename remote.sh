#!/usr/bin/env bash

# Do not edit theese variables
SCRIPT_DIR="$(dirname $(readlink -f $0))"
CONFIG_DIR="$SCRIPT_DIR/bin"
HOSTS_DIR="$SCRIPT_DIR/hosts"

# Check if dependencies are met
function dependency_check() {
	local PACKAGES=(bash wc basename dirname readlink head tail sed grep)
	local MISSING=""
	for i in "${PACKAGES[@]}"; do
		if [ "$(command -v $i)" == "" ]; then
			MISSING="$MISSING $i"
		fi
	done
	if [ "$MISSING" != "" ]; then
		if [ "$(echo $MISSING | wc -w)" == "1" ]; then
			echo "Missing package:$MISSING"
		else
			echo "Missing packages:$MISSING"
		fi
		exit
	fi
}
dependency_check

function menu_list_tools() {
	local LS=$(ls $CONFIG_DIR/tools | sed "s/.sh//g")
	echo "Total: $(echo $LS | wc -w)"
	echo ""
	echo ${LS^^} | sed "s/ /\n/g"
}
# Load all tools
for i in $CONFIG_DIR/tools/*.sh; do
	source $i
done
# Ensure that HOSTS_DIR exists
if [ ! -d "$HOSTS_DIR" ]; then
	mkdir $HOSTS_DIR
fi

menu_help() {
	cat "$CONFIG_DIR"/help.txt
}

# Argument interpreter
if [ $# == "0" ]; then
	echo "No arguments given, use \"help\" or \"h\" for help"
	exit
fi
case $1 in
	h | help)
		menu_help "$2"
		exit
		;;
	start)
		menu_start "$@"
		exit
		;;
	stop)
		menu_stop "$@"
		exit
		;;
	enable)
		menu_enable "$@"
		exit
		;;
	disable)
		menu_disable "$@"
		exit
		;;
	r | restart)
		menu_restart "$@"
		exit
		;;
	kill)
		menu_kill "$@"
		exit
		;;
	unlock)
		menu_unlock "$@"
		exit
		;;
    test)
        menu_test "$@"
        exit
        ;;
	ls | list)
		menu_list "$@"
		exit
		;;
	tools)
		menu_list_tools
		exit
		;;
	*)
		echo "Invalid argument, use \"help\" or \"h\" for help"
		exit
		;;
esac
