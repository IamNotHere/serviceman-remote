#!/usr/bin/env bash

# Test ssh connection and SMAN installation status
function menu_test() {
	shift
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	local REPLY

	# Handle adhoc
	if [ "$1" == "adhoc" ]; then
		shift

		REPLY="$(ssh "$1" echo CONNECTION_TEST)"

		if [ "$REPLY" != "CONNECTION_TEST" ]; then
			echo "Connection failed"
			exit
		fi

		REPLY="$(ssh -t "$1" /bin/bash -ic 'alias sman' | grep 'sman.sh')"

		echo "TESTING..."

		if [ "$REPLY" != "" ]; then
			local BIN
			BIN="$(ssh -t "$1" /bin/bash -ic 'alias sman')"
			BIN="$(echo "$BIN" | grep 'sman.sh' | awk 'BEGIN { FS="=" } { print $2 }' | sed s/\'//g | awk '{ print $2 }' | tr -d '\r')"
			echo "Connection sucessful, SMAN installed in $BIN"
		else
			echo "Connection sucessful, SMAN is NOT installed"
		fi

		exit
	fi

	local HOST_LIST

	if [ ! -f "$HOSTS_DIR/$1" ]; then
		echo "Invalid host list file"
		exit
	fi
	HOST_LIST="$(cat "$HOSTS_DIR/$1")"
	for i in $HOST_LIST; do
		echo -e "\nTESTING: $i"
		REPLY="$(ssh "$i" echo CONNECTION_TEST)"

		if [ "$REPLY" != "CONNECTION_TEST" ]; then
			echo "Connection failed"
		else

			REPLY="$(ssh -t "$i" /bin/bash -ic 'alias sman' | grep 'sman.sh')"

			if [ "$REPLY" != "" ]; then
				echo "Connection sucessful, SMAN installed"
			else
				echo "Connection sucessful, SMAN is NOT installed"
			fi
		fi
	done

	unset HOST_LIST
	unset REPLY
}
