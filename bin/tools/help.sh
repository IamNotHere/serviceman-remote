#!/usr/bin/env bash

# Help message
function menu_help() {
	if [ "$1" == "" ]; then
		echo "$MSG_HELP"
	else
		case $1 in
		autostart)
			echo "To utilize the autostart services function you have to add this line to your crontab file (crontab -e):

@reboot bash $SCRIPT_DIR/$(basename $"(readlink -f $0)") perform-autostart
		"
			exit
			;;
		command)
			local ALIAS_COMMAND="alias sman=\"bash $SCRIPT_DIR/$(basename $(readlink -f $0))\""
			local SEARCH="$(cat "$HOME/.bash_aliases" | grep -e "^$ALIAS_COMMAND$" | wc -l)"
			if [ "$SEARCH" == "0" ]; then
				echo "Do You want to add a global command (alias \"sman\")? [Y/n]"
				read ANS
				if [ "$ANS" == "" -o "$ANS" == "Y" -o "$ANS" == "y" ]; then

					if [ "$SHELL" == "/bin/bash" ]; then
						SH_DIR=$HOME/.bash_aliases
						if [ ! -f "$HOME/.bash_aliases" ]; then
							touch "$SH_DIR"
						fi
					elif [ "$SHELL" == "/bin/zsh" ]; then
						SH_DIR=$HOME/.zshrc
						if [ ! -f "$HOME/.zshrc" ]; then
							touch "$SH_DIR"
						fi
					else
						echo "Your shell version is not supported."
					fi

					echo "" >>$HOME/.bash_aliases
					echo "# This is a programatically added alias for serviceman" >>$HOME/.bash_aliases
					echo "$ALIAS_COMMAND" >>$HOME/.bash_aliases

					echo "The command syntax is: sman <args>"
					echo "Type \"source $HOME/.bash_aliases\" or relog/reopen your terminal emulator for the changes to take effect"
				fi
			else
				echo "The alias is already present"
			fi
			exit
			;;
		*)
			echo "Unknown manual page"
			exit
			;;
		esac
	fi
}
