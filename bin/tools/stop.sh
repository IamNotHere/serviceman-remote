#!/usr/bin/env bash

# Stop service
function menu_start() {
	shift
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	local REPLY

	# Handle adhoc
	if [ "$1" == "adhoc" ]; then
		shift

		# Get SMAN location
		local BIN
		BIN="$(ssh -t "$1" /bin/bash -ic 'alias sman')"
		BIN="$(echo "$BIN" | grep 'sman.sh' | awk 'BEGIN { FS="=" } { print $2 }' | sed s/\'//g | awk '{ print $2 }' | tr -d '\r')"

		REPLY="$(ssh "$1" "$BIN" stop "$2")"

		if [ "$REPLY" == "" ]; then
			echo "Success"
		else
			echo "ERROR: $REPLY"
		fi

		exit
	fi

	local HOST_LIST

	if [ ! -f "$HOSTS_DIR/$1" ]; then
		echo "Invalid host list file"
		exit
	fi
	if [ "$(cat "$HOSTS_DIR/$1")" == "" ]; then
		echo "Empty host list file"
		exit
	fi
	HOST_LIST="$(cat "$HOSTS_DIR/$1")"
	for i in $HOST_LIST; do
		echo ""
		REPLY="$(ssh "$i" echo CONNECTION_TEST)"

		if [ "$REPLY" != "CONNECTION_TEST" ]; then
			echo "Connection failed to $i"
			continue
		fi

		# Get SMAN location
		local BIN
		BIN="$(ssh -t "$i" /bin/bash -ic 'alias sman')"
		BIN="$(echo "$BIN" | grep 'sman.sh' | awk 'BEGIN { FS="=" } { print $2 }' | sed s/\'//g | awk '{ print $2 }' | tr -d '\r')"

		REPLY="$(ssh "$i" "$BIN" stop "$2")"

		if [ "$REPLY" == "" ]; then
			echo "Success"
		else
			echo "ERROR: $REPLY"
		fi
	done

	unset HOST_LIST
	unset REPLY
}
