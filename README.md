# Serviceman
Serviceman Remote is a small remote controller script made to interface with Serviceman.

<p style="text-align: center !important;">
  <img width="500" src="img/logo.svg">
</p>

## Warning

This script expects that all hosts that its expected to connect to use ssh key - passwordless authentication (can be defined in `~/.ssh/config`)

#### Help page contents

	Syntax:
		remote <action> <destination> <args>
		remote <action> adhoc <host> <args>

	ACTIONS:
		help | h		Display this help message

		test			Test connection to a host

		start			Start service, "start all" to start all services

		restart			Restart a service, the service will be locked for the time it takes to restart it

		stop			Stop service, "stop all" to stop all services

		enable			Enable service for automatic onboot startup

		disable			Disable service from automatic startup

		kill			Kill a service, this command uses SIGKILL (-9) to kill the service process.
						Usage: kill <service name>

		unlock			Forecefully unlock a service in the event that
						for some reason serviceman is not able to unlock it by itself

		tools			List loaded tools

	DESTINATION:

		To run the remote on one host only use "adhoc <hostname>" as the destination.
		Otherwise specify a filename of a host list file located in 'hosts'